""" The module use for parse console commands."""

import argparse


def parse(input_args):
    """ Function, which parse CLI
    example usage '-c -in ./data/input.txt -out ./data/output.txt'
    :param input_args:
    :return:
    """
    parser = argparse.ArgumentParser(
        description='This program is console Drawing tool.',
        add_help=False)
    parser.add_argument('-c', '--console', action="store_false", default=True,
                        help='use it for using with CMD interface.\r\n')
    parser.add_argument('-in', '--input', type=str, help='input file path.\r\n')
    parser.add_argument('-out', '--output', type=str, help='output file path.\r\n', default='./output.txt')
    parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                        help='Show help message and exit.\r\n')
    try:
        return parser.parse_args(args=input_args)
    except argparse.ArgumentError:
        print('Use -h option for help on using.')
