""" The module contains of a class Canvas."""

LINE_SYMBOL = 'x'


class Canvas:
    """
    A class used to represent an Canvas.
    The constructor takes the length and width of the canvas.
    """

    def __init__(self, params):
        self.width = int(params[0]) + 2
        self.height = int(params[1]) + 2
        self.data = []
        for i in range(self.height):
            temp = []
            for j in range(self.width):
                if i in (0, self.height - 1):
                    temp.append('-')
                else:
                    temp.append('|') if j in (0, self.width - 1) else temp.append(' ')
            self.data.append(temp)

    def __str__(self):
        return '\n'.join([''.join(item) for item in self.data])

    def fill(self, pos_x, pos_y, change_color, original_color):
        """
        Recurrent function that colors the selected position (x, y).
        :param pos_x: int
        :param pos_y: int
        :param change_color: char
        :param original_color: char
        """
        if pos_x < 0 or pos_y < 0: return
        color = self.data[pos_y][pos_x]
        if color in ('-', '|', change_color): return
        if color is not original_color: return
        self.data[pos_y][pos_x] = change_color

        self.fill(pos_x, pos_y - 1, change_color, original_color)  # top
        self.fill(pos_x, pos_y + 1, change_color, original_color)  # bottom
        self.fill(pos_x - 1, pos_y, change_color, original_color)  # lef
        self.fill(pos_x + 1, pos_y, change_color, original_color)  # right

    def bucket_fill(self, params):
        """
        Fill the entire area connected to (x,y) with "colour" c.
        :param params: [x, y, change_symbol]
        """
        pos_x, pos_y = map(int, params[:-1])
        change_symbol = params[-1]
        original_color = self.data[pos_y][pos_x]
        self.fill(pos_x, pos_y, change_symbol, original_color)

    def write_rectangle(self, params):
        """
        Write a new rectangle, whose upper left corner is (x_1, y_1) and lower
        right corner is (x_2, y_2).
        :param params: [x_1, y_1, x_2, y_2]
        """
        x_1, y_1, x_2, y_2 = params
        self.write_line([x_1, y_1, x_2, y_1])  # up
        self.write_line([x_1, y_2, x_2, y_2])  # down
        self.write_line([x_1, y_1, x_1, y_2])  # left
        self.write_line([x_2, y_1, x_2, y_2])  # right

    def write_line(self, params):
        """
        Write a new line from (x_1, y_1) to (x_2, y_2).
        Currently only horizontal or vertical lines. Horizontal and vertical lines
        will be drawn using the 'x' character.
        :param params: [x_1, y_1, x_2, y_2]
        """
        x_1, y_1, x_2, y_2 = map(int, params)
        height = self.height - 1
        height = self.width - 1
        if 0 in (x_1, y_1, x_2, y_2) or height < y_1 or height <= y_2 or height <= x_1 or height <= x_2:
            raise IndexError('Went abroad canvas, and there you want to paint?')
        good_line = False
        if x_1 == x_2:
            good_line = True
            for i in range(y_1, y_2 + 1):
                self.data[i][x_1] = LINE_SYMBOL

        if y_1 == y_2:
            good_line = True
            for j in range(x_1, x_2 + 1):
                self.data[y_2][j] = LINE_SYMBOL
        if not good_line: raise ValueError('Only horizontal or vertical lines are supported')
