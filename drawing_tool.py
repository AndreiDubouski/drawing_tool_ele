""" Main script Drawing tool. """
import cmd, sys
import parser_cmd as parser
import canvas


def analyse(line, picture=None):
    """
    Analyse input string line.
    :param line: str
    :param picture:
    :return:
    """
    command, *params = line.split(sep=' ')
    try:
        if command == 'C':
            picture = canvas.Canvas(params)  # C w h
        elif command == 'L':
            picture.write_line(params)  # L x1 y1 x2 y2
        elif command == 'R':
            picture.write_rectangle(params)  # R x1 y1 x2 y2
        elif command == 'B':
            picture.bucket_fill(params)  # B x y c
    except (AttributeError, ValueError, IndexError):
        print('Some error with command')

    return picture


class Shell(cmd.Cmd):
    intro = 'Welcome to the Drawing tool shell.   Type help or ? to list commands.\n'
    prompt = '(Drawing tool) '
    picture = None

    def do_C(self, arg):
        'Create a new canvas of width w and height h: C 20 4'
        try:
            params = arg.split(sep=' ')
            self.picture = canvas.Canvas(params)
            print(self.picture)
        except ValueError:
            print('Some error with parameters, use: "help C" to see how the function is used.')

    def do_L(self, arg):
        'Create a new line from (x1,y1) to (x2,y2). Currently only horizontal\
         or vertical lines are supported: L 1 2 6 2'
        try:
            params = arg.split(sep=' ')
            self.picture.write_line(params)
            print(self.picture)
        except AttributeError:
            print('You can only draw if a canvas has been created.')
        except (ValueError, IndexError):
            print('Some error with command, use: "help L" to see how the function is used.')

    def do_R(self, arg):
        'Create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2): R 16 1 20 3'
        try:
            params = arg.split(sep=' ')
            self.picture.write_rectangle(params)
            print(self.picture)
        except AttributeError:
            print('You can only draw if a canvas has been created.')
        except (ValueError, IndexError):
            print('Some error with command, use: "help R" to see how the function is used.')

    def do_B(self, arg):
        'Fill the entire area connected to (x,y) with "colour" c: B 10 3 o'
        try:
            params = arg.split(sep=' ')
            self.picture.bucket_fill(params)
            print(self.picture)
        except AttributeError:
            print('You can only draw if a canvas has been created.')
        except (ValueError, IndexError):
            print('Some error with command, use: "help B" to see how the function is used.')

    def do_close(self, arg):
        "Stop Drawing tool, close the window, and exit:  close"
        print('Thank you for using Drawing tool')
        return True


if __name__ == '__main__':
    ARG = sys.argv[1:]
    PARSED_ARG = parser.parse(ARG)
    if not PARSED_ARG.console:
        picture = None
        with open(PARSED_ARG.input, 'r') as f_in:
            for read_line in f_in:
                picture = analyse(read_line.rstrip(), picture)
                print(picture)
                with open(PARSED_ARG.output, 'a+') as f_out:
                    f_out.write(str(picture) + '\n')

        print('Done write to file')
    else:
        Shell().cmdloop()
